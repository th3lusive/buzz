# Build

`zig build -Drelease-small --prefix ~/.local install`

# Setup:

- Bash: add the following to `~/.bashrc`
  `eval "$(buzz init bash)"`

- Zsh: add the following to `~/.zshrc`
  `eval "$(buzz init zsh)"`

- Fish: add the following to `~/.config/fish/config.fish`
  `buzz init fish | source`
