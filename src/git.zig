const std = @import("std");
const Colors = @import("Colors.zig");
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

// TODO: use `zig-libgit2` to get the current branch name, Unable to do
// this in a cross-platform method due to libc headers not available;
// compilation does not link against libc

fn getBranchName(allocator: Allocator, path: []const u8) !?[]u8 {
    const result = try std.ChildProcess.exec(.{
        .allocator = allocator,
        .argv = &[_][]const u8{ "git", "branch", "--show-current" },
        .cwd = path,
    });

    const size = result.stdout.len;
    if (size < 2) return null;
    return result.stdout[0 .. size - 1]; // remove trailing newline
}

test "getBranchName" {
    var arena = std.heap.ArenaAllocator.init(std.testing.allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const branch = try getBranchName(allocator, ".");
    try std.testing.expectEqualStrings(branch.?, "master");
}

fn getBranchStatus(allocator: Allocator, path: []const u8) !?[]u8 {
    const result = try std.ChildProcess.exec(.{
        .allocator = allocator,
        .argv = &[_][]const u8{ "git", "status", "--porcelain" },
        .cwd = path,
    });

    if (result.stdout.len < 2) return null;
    return result.stdout;
}

pub fn makePrompt(allocator: Allocator, path: []const u8) !?[]u8 {
    const branch = try getBranchName(allocator, path);
    const status = try getBranchStatus(allocator, path);
    if (branch == null) return null;

    var sign = ArrayList(u8).init(allocator);
    const set = Colors.set;
    const a = allocator;

    try sign.appendSlice(" (");
    try sign.appendSlice(try set(a, .Cyan, branch.?));

    if (status == null) {
        try sign.appendSlice(")");
        return sign.toOwnedSlice();
    }

    // zig fmt: off
    const contains = std.mem.containsAtLeast;
    const added     = contains(u8, status.?, 1, "A");
    const deleted   = contains(u8, status.?, 1, "D");
    const renamed   = contains(u8, status.?, 1, "R");
    const modified  = contains(u8, status.?, 1, "M");
    const untracked = contains(u8, status.?, 1, "?");
    // zig fmt: on

    if (added) try sign.appendSlice(try set(a, .Cyan, "+"));
    if (deleted) try sign.appendSlice(try set(a, .Red, "-"));
    if (renamed) try sign.appendSlice(try set(a, .Yellow, "~"));
    if (modified) try sign.appendSlice(try set(a, .BrightYellow, "*"));
    if (untracked) try sign.appendSlice(try set(a, .BrightRed, "?"));
    try sign.appendSlice(")");

    return sign.toOwnedSlice();
}
