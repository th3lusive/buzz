#!/usr/bin/env bash

buzz_prompt_command() {
    echo -en "\033]0;bash $(pwd)\a"
	PS1='$(buzz --status $?)'
}

PROMPT_COMMAND=buzz_prompt_command
