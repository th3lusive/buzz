autoload -Uz add-zsh-hook
_buzz_prompt() {
	PROMPT=$(buzz --status $?)
}

add-zsh-hook precmd _buzz_prompt
