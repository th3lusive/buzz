const std = @import("std");
const git = @import("git.zig");
const path = @import("path.zig");
const eql = std.mem.eql;
const Colors = @import("Colors.zig");
const Allocator = std.mem.Allocator;

const bash_prompt = @embedFile("./init/buzz.bash");
const zsh_prompt = @embedFile("./init/buzz.zsh");
const fish_prompt = @embedFile("./init/buzz.fish");

pub fn range(len: usize) []const void {
    // https://github.com/nektro/zig-range
    return @as([*]void, undefined)[0..len];
}

fn makePrompt(args: struct {
    allocator: Allocator,
    status: usize,
    sym: []const u8,
    cwd: []const u8,
}) ![]u8 {
    const allocator = args.allocator;
    const cwd = args.cwd;

    const dir = blk: {
        const truncated = try path.truncatePath(allocator, cwd, std.os.getenv("HOME").?);
        const dir = try Colors.set(allocator, .Magenta, truncated);
        break :blk dir;
    };

    const vcs = blk: {
        const gitprompt = try git.makePrompt(allocator, cwd);
        const vcs = gitprompt orelse "";
        break :blk vcs;
    };

    const sym = blk: {
        var sym = if (eql(u8, args.sym, "")) ">" else args.sym;
        if (args.status != 0)
            sym = try Colors.set(allocator, .Red, sym);
        break :blk sym;
    };

    const prompt = blk: {
        var prompt = try std.fmt.allocPrint(
        allocator,
        "{s}{s}{s} ",
        .{ dir, vcs, sym },
    );
        break :blk prompt;
    };

    return prompt;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const allocator = arena.allocator();
    const args = try std.process.argsAlloc(allocator);
    const stdout = std.io.getStdOut().writer();

    var status: usize = 0;
    var symbol = std.ArrayList(u8).init(allocator);
    for (range(args.len)) |_, i| {
        if (i == 0) continue;
        if (eql(u8, args[i], "init")) {
            if (eql(u8, args[i + 1], "bash")) {
                try stdout.print("{s}", .{bash_prompt});
            } else if (eql(u8, args[i + 1], "zsh")) {
                try stdout.print("{s}", .{zsh_prompt});
            } else if (eql(u8, args[i + 1], "fish")) {
                try stdout.print("{s}", .{fish_prompt});
            } else {
                try stdout.print("{s}", .{bash_prompt});
            }

            return;
        }
        if (eql(u8, args[i], "--status")) {
            status = try std.fmt.parseInt(usize, args[i + 1], 10);
            continue;
        }

        if (eql(u8, args[i], "--symbol")) {
            try symbol.appendSlice(args[i + 1]);
            continue;
        }
    }

    const cwd = try std.fs.cwd().realpathAlloc(allocator, ".");
    const result = try makePrompt(.{
        .allocator = allocator,
        .status = status,
        .sym = symbol.items,
        .cwd = cwd,
    });

    try stdout.print("{s}\n", .{result});
}
